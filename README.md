# README #

Welcome. This is a work in progress.

# Project summary

I've been obsessed lately with something along the lines of last-mile personal mobility. 

While I'd love to manifest a physical solution (a vehicle, device, clothing item, process, or system) I'm going to try to start by modelling the problem as a game and work through solutions without the constraints of the real world. 

* At the moment, maybe there's an idea for a game here.

## A start

Modelling the problem.

## To drive or not to drive

There are some costs and benefits around owning and/or maintaining a car, participating in ride-sharing, public transportation, which I'd like to include in the model. Also, there are some limitations and/or risks associated with riding a bicycle or motorcycle as an alternative or complement to a car.

Mode of travel is a factor to model: Driving vs. flying vs. riding vs. walking. 

These bring health pros and cons, budgetary considerations, risks associated with accident, weather and distance variables, various mainteance costs, and a time expenditure. (Other factors?)

## Environment (A -> B)

Travel between A->B is influenced by the shape of the living, working, playing arrangement and the proximity, timing, costs, and availability of different activities we want or need to participate in. 

Consider modelling the environment. While the cost of this in the physical world might make this improbably expensive in terms of currency costs and/or political will, modelling this in game space might be a helpful (or at least fun) exercise. 

### Proximity 

Proximity of A->B seems to be the primary consideration. Moving these closer to each other seems like an obvious approach to a solution. However, it's probably not so simple. 

For example, I'd like to spend more time at the beach, but I'm concerned about storms, tides, and god forbid, a tsunami which in an unlikely but possible outcome. Therefore, I'd like to live close, but outside of a zone prone to such risks. And, should an emergency need to evacuate the beach arise, consideration for a proportionally appropriate exodus ought to be provided in the modelled variables.

Some other factors resisting moving A->B closer together could be geological features such as mountains and rivers. 

Land usage management such as water treatment, watershed, eco-habitation, accomodating for air traffic needs and/or defense, etc. 

What am I missing?


## Laundry list of todos

* [ ] Scope my problem space better. 
	* Urban/Rural? 
	* Ground/Flight/Robotic or Remote Surrogate/Teleportation? 
	* A->B for Work/Play/Exploration/Maintenance? 
	* Single/Multiplayer? (Population density?)
	* Limit to physically possible?
	* If this is going to be purely in the realm a gaming a solution, maybe its more interesting without the constraints of physics?

...continue brainstorming this
